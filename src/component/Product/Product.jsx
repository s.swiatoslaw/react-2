import {Component} from 'react'
import Modal from '../Modal';
import Button from '../Button';
import FavoriteIcon from '@material-ui/icons/Favorite'
import './Product.scss'

class Product extends Component {
    constructor() {
        super();
        this.state = {
            isAddedToCart: false,
            isOpenModal: false,
            isFavorite: false,
            colorFavorite: "#bdbdbd",
            colorCart: ""
        }
    }

    toggleModal = () =>{
        this.setState({
          isOpenModal: !this.state.isOpenModal
        })
      }
      
    sendProductId = () => {
        this.setState({isAddedToCart: true, colorCart: "#3f51b5"})
        this.props.sendId(this.props.id);
    }
    
    addFavorite = () => {
        this.setState({
            isFavorite: !this.isFavorite,
            colorFavorite: "#3f51b5"
        })
        if (this.state.colorFavorite === "#3f51b5") {
            this.setState({colorFavorite: "#bdbdbd"})
        }
    }

    render() {
        const {name, price, image, article, color, openModal} = this.props;
        return (
            <>
            {this.state.isOpenModal && (
                <Modal
                closeModal={this.toggleModal}
                header="Добавить товар в корзину?"
                text="Вы действительно хотите добавить этот товар в корзину?"
                actions={
                    <>
                    <Button onclick={this.toggleModal} text="Нет"/>
                    <Button onclick={this.sendProductId} text="Да"/>
                    </>
                }
                />
            )}
            <div id={article} className="product">
                <img src={image} alt={name} />
                <FavoriteIcon onClick={this.addFavorite} className="favorite-icon" style={{ color: this.state.colorFavorite }} />
                <div className="product-text">
                    <h1>{name}</h1>
                    <p>{price}$</p>
                </div> 
                {this.state.isAddedToCart ? 
                    (
                        <Button onclick={this.toggleModal} text="Добавлено в корзину" color={this.state.colorCart} colorText="white"/>            
                    ) : (
                        <Button onclick={this.toggleModal} text="В корзину"/>            
                    )
                } 
            </div>
            </>
        )
    }
}

export default Product