import {Component} from "react"
import './Button.scss'

class Button extends Component {
    render() {
        const {text, onclick, color, colorText} = this.props;
        return <button style={{ background: color, color: colorText}} onClick={onclick}>{text}</button>
    }
}

export default Button