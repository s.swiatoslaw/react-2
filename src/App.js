import './App.scss';
import {Component, useEffect, useState} from 'react'
import getProducts from './api/getProducts'
import Product from './component/Product/Product'

class App extends Component {
  state = {
    products: [],
    cart: []
  }


  componentDidMount() {
    const getCartLocal = localStorage.getItem('cart');
    console.log(getCartLocal)
    if (getCartLocal !== null) {
      this.setState({
        cart: JSON.parse(localStorage.getItem('cart'))
      })
    }
    getProducts().then(response => {
      this.setState({
        products: response.data
      })
    })
  }

  addToCart = (value) => {
    this.setState((state) => {
      return {
        cart: [...state.cart, value]
      }
    })
    localStorage.setItem('cart', JSON.stringify(this.state.cart));
  }

  render() {
    const productList = this.state.products;

    return (
      <>
      <div className="product-list">
        {productList.map((item, key) => {
            return (
              <>
                <Product
                  id={key}
                  name={item.name}
                  price={item.price}
                  image={item.image}
                  article={item.article}
                  sendId={this.addToCart}
                />
              </>
            )
          } 
        )}
        </div>
      </>
    )
  }
}

export default App;
