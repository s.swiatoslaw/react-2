import axios from 'axios'

const getProducts = () => {
    return axios('./products.json')
}

export default getProducts